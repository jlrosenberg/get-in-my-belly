# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

MeasurementUnit.create(singular_name: "Gram", plural_name: "Grams", in_grams: 1, created_at: DateTime.new(2019, 1, 1))
MeasurementUnit.create(singular_name: "Cup", plural_name: "Cups", in_grams: 128, created_at: DateTime.new(2019, 1, 1))
MeasurementUnit.create(singular_name: "Ounce", plural_name: "Ounces", in_grams: 28.35, created_at: DateTime.new(2019, 1, 1))
MeasurementUnit.create(singular_name: "tsp", plural_name: "tsps", in_grams: 4.92)
MeasurementUnit.create(singular_name: "tbsp", plural_name: "tbsps", in_grams: 15)
MeasurementUnit.create(singular_name: "count", plural_name: "count", in_grams: 1)

Ingredient.create(name: "Flour", measurement_unit_id: 1)
Ingredient.create(name: "Egg", measurement_unit_id: 6)
Ingredient.create(name: "Water", measurement_unit_id: 2)
Ingredient.create(name: "Butter", measurement_unit_id: 5)
Ingredient.create(name: "Milk", measurement_unit_id: 2)
Ingredient.create(name: "Sugar", measurement_unit_id: 2)

User.create(email: "person@gmail.com", password: "password", password_confirmation: "password",  
username: "JohnCook123", first_name: "John", last_name: "Doe", profile_picture_url: "")
User.create(email: "janeDoe@gmail.com", password: "password", password_confirmation: "password",
username: "JaneCooking", first_name: "Jane", last_name: "Doe", profile_picture_url: "")

Recipe.create(name: "Pancakes", description: "Some old-fashioned yummy pancakes.", banner_image_url: "https://images-gmi-pmc.edge-generalmills.com/edfaaf9f-9bde-426a-8d67-3284e9e496ae.jpg", 
author_id: "1")
Recipe.create(name: "Milky Pancake", description: "A milky pancake.", banner_image_url: "https://prettysimplesweet.com/wp-content/uploads/2014/08/ButtermilkPancakes.jpg",
author_id: "2")
Recipe.create(name: "Scrambled Eggs", description: "Grandmas classic scrambled eggs", banner_image_url: "https://www.thespruceeats.com/thmb/NCvH1nmLr9Crg8f5bIgCnyJszXM=/5000x2812/smart/filters:no_upscale()/scrambled-eggs-58a701ac5f9b58a3c91cbebd.jpg",
  author_id: "2")

RecipeStep.create(description: "Mix Flour, Egg, and Water", order: 1, recipe_id: 1)
RecipeStep.create(description: "Coat pan in Butter", order: 2, recipe_id: 1)
RecipeStep.create(description: "Fry in pan until golden cripsy brown", order: 3, recipe_id: 1)
RecipeStep.create(description: "Mix Flour, Egg, and Milk", order: 1, recipe_id: 2)
RecipeStep.create(description: "Coat pan in Butter", order: 2, recipe_id: 2)
RecipeStep.create(description: "Fry in pan until golden crispy brown", order: 3, recipe_id: 2)
RecipeStep.create(description: "Crack the eggs into a bowl. Make sure there are no shells in the bowl.", order: 1, recipe_id: 3)
RecipeStep.create(description: "Add the milk to the bowl and whisk until thoroughly combined", order: 2, recipe_id: 3)
RecipeStep.create(description: "Pour the egg mixture into the pan. Continue stirring until done. Add cheese. Then serve", order: 3, recipe_id: 3)


Tag.create(name: "Classic")
Tag.create(name: "Easy")

RecipeTagRelationship.create(recipe_id: 1, tag_id:1)
RecipeTagRelationship.create(recipe_id: 1, tag_id:2)
RecipeTagRelationship.create(recipe_id: 2, tag_id:1)

RecipeIngredientRelationship.create(recipe_id: 1, ingredient_id: 1, quantity: 1, measurement_unit_id: 2)
RecipeIngredientRelationship.create(recipe_id: 1, ingredient_id: 2, quantity: 4, measurement_unit_id: 6)
RecipeIngredientRelationship.create(recipe_id: 1, ingredient_id: 3, quantity: 3, measurement_unit_id: 4)
RecipeIngredientRelationship.create(recipe_id: 1, ingredient_id: 4,  quantity: 50, measurement_unit_id: 3)

RecipeIngredientRelationship.create(recipe_id: 2, ingredient_id: 1, quantity: 500, measurement_unit_id: 1)
RecipeIngredientRelationship.create(recipe_id: 2, ingredient_id: 2, quantity: 3, measurement_unit_id: 6)
RecipeIngredientRelationship.create(recipe_id: 2, ingredient_id: 4, quantity: 50, measurement_unit_id: 1)
RecipeIngredientRelationship.create(recipe_id: 2, ingredient_id: 5, quantity: 100, measurement_unit_id: 1)

RecipeIngredientRelationship.create(recipe_id: 3, ingredient_id: 1, quantity: 500, measurement_unit_id: 1)
RecipeIngredientRelationship.create(recipe_id: 3, ingredient_id: 2, quantity: 3, measurement_unit_id: 6)



