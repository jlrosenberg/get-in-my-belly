# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_12_03_005558) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "ingredients", force: :cascade do |t|
    t.string "name", null: false
    t.bigint "measurement_unit_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["measurement_unit_id"], name: "index_ingredients_on_measurement_unit_id"
    t.index ["name"], name: "index_ingredients_on_name", unique: true
  end

  create_table "measurement_units", force: :cascade do |t|
    t.string "singular_name", null: false
    t.string "plural_name", null: false
    t.float "in_grams", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "recipe_ingredient_relationships", force: :cascade do |t|
    t.integer "recipe_id", null: false
    t.integer "ingredient_id", null: false
    t.float "quantity", null: false
    t.integer "measurement_unit_id", null: false
  end

  create_table "recipe_steps", force: :cascade do |t|
    t.string "description", null: false
    t.integer "order", null: false
    t.bigint "recipe_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["recipe_id"], name: "index_recipe_steps_on_recipe_id"
  end

  create_table "recipe_tag_relationships", force: :cascade do |t|
    t.integer "recipe_id", null: false
    t.integer "tag_id", null: false
  end

  create_table "recipes", force: :cascade do |t|
    t.string "name", null: false
    t.string "description"
    t.string "banner_image_url"
    t.bigint "author_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_id"], name: "index_recipes_on_author_id"
  end

  create_table "tags", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_tags_on_name", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "username"
    t.string "first_name"
    t.string "last_name"
    t.string "profile_picture_url"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  add_foreign_key "ingredients", "measurement_units"
  add_foreign_key "recipe_ingredient_relationships", "ingredients"
  add_foreign_key "recipe_ingredient_relationships", "measurement_units"
  add_foreign_key "recipe_ingredient_relationships", "recipes"
  add_foreign_key "recipe_tag_relationships", "recipes"
  add_foreign_key "recipe_tag_relationships", "tags"
  add_foreign_key "recipes", "users", column: "author_id"
end
