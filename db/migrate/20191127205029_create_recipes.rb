class CreateRecipes < ActiveRecord::Migration[5.2]
  def change
    create_table :recipes do |t|
      t.string :name, null: false
      t.string :description
      t.string :banner_image_url
      t.references :author, foreign_key: {to_table: :users}, null: false
      t.timestamps
    end

    create_table :recipe_ingredient_relationships do |t|
      t.integer :recipe_id, null: false
      t.integer :ingredient_id, null: false
      t.float :quantity, null: false
      t.integer :measurement_unit_id, null:false
    end

    create_table :recipe_tag_relationships do |t|
      t.integer :recipe_id, null:false
      t.integer :tag_id, null:false
    end
  end
end
