class AddRecipeSteps < ActiveRecord::Migration[5.2]
  def change
    create_table :recipe_steps do |t|
      t.string :description, null: false
      t.integer :order, null: false
      t.references :recipe, null: false
      t.timestamps
    end
  end
end
