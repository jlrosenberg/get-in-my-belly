class CreateIngredients < ActiveRecord::Migration[5.2]
  def change
    create_table :ingredients do |t|
      t.string :name, null:false, unique:true
      t.references :measurement_unit, foreign_key: true, null:false

      t.timestamps
    end
  end
end
