class AddForeignKeysToRelationshipTables < ActiveRecord::Migration[5.2]
  def change
    add_foreign_key :recipe_ingredient_relationships, :recipes
    add_foreign_key :recipe_ingredient_relationships, :ingredients
    add_foreign_key :recipe_ingredient_relationships, :measurement_units

    add_foreign_key :recipe_tag_relationships, :tags
    add_foreign_key :recipe_tag_relationships, :recipes
  end
end
