class CreateMeasurementUnits < ActiveRecord::Migration[5.2]
  def change
    create_table :measurement_units do |t|
      t.string :singular_name, null:false
      t.string :plural_name, null:false
      t.float :in_grams, null:false

      t.timestamps
    end
  end
end
