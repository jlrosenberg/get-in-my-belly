Rails.application.routes.draw do


  root 'homepage#index'
  get 'recipes/new', to: 'recipes#new'
  # get 'recipes/:id', to: 'homepage#index'

  resources :measurement_units
  resources :ingredients
  resources :tags
  resources :recipes
  resources :recipe_tag_relationships
  resources :recipe_ingredient_relationships
  resources :recipe_steps
  resources :users

  scope :api, defaults: {format: :json} do
    scope :v1 do
      devise_for :users,  controllers: { sessions: :sessions },
                          path_names: { sign_in: :login }

      resource :user, only: [ :update]
      resources :measurement_units
      resources :ingredients, only: [ :create, :index, :show]
      resources :tags, only: [:create, :index, :show]
      resources :recipes


      get '/users/:id', to: 'users#show'
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
