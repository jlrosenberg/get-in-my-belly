require "application_system_test_case"

class RecipeIngredientRelationshipsTest < ApplicationSystemTestCase
  setup do
    @recipe_ingredient_relationship = recipe_ingredient_relationships(:one)
  end

  test "visiting the index" do
    visit recipe_ingredient_relationships_url
    assert_selector "h1", text: "Recipe Ingredient Relationships"
  end

  test "creating a Recipe ingredient relationship" do
    visit recipe_ingredient_relationships_url
    click_on "New Recipe Ingredient Relationship"

    click_on "Create Recipe ingredient relationship"

    assert_text "Recipe ingredient relationship was successfully created"
    click_on "Back"
  end

  test "updating a Recipe ingredient relationship" do
    visit recipe_ingredient_relationships_url
    click_on "Edit", match: :first

    click_on "Update Recipe ingredient relationship"

    assert_text "Recipe ingredient relationship was successfully updated"
    click_on "Back"
  end

  test "destroying a Recipe ingredient relationship" do
    visit recipe_ingredient_relationships_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Recipe ingredient relationship was successfully destroyed"
  end
end
