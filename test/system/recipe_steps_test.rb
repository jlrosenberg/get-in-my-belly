require "application_system_test_case"

class RecipeStepsTest < ApplicationSystemTestCase
  setup do
    @recipe_step = recipe_steps(:one)
  end

  test "visiting the index" do
    visit recipe_steps_url
    assert_selector "h1", text: "Recipe Steps"
  end

  test "creating a Recipe step" do
    visit recipe_steps_url
    click_on "New Recipe Step"

    click_on "Create Recipe step"

    assert_text "Recipe step was successfully created"
    click_on "Back"
  end

  test "updating a Recipe step" do
    visit recipe_steps_url
    click_on "Edit", match: :first

    click_on "Update Recipe step"

    assert_text "Recipe step was successfully updated"
    click_on "Back"
  end

  test "destroying a Recipe step" do
    visit recipe_steps_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Recipe step was successfully destroyed"
  end
end
