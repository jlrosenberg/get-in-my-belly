require "application_system_test_case"

class RecipeTagRelationshipsTest < ApplicationSystemTestCase
  setup do
    @recipe_tag_relationship = recipe_tag_relationships(:one)
  end

  test "visiting the index" do
    visit recipe_tag_relationships_url
    assert_selector "h1", text: "Recipe Tag Relationships"
  end

  test "creating a Recipe tag relationship" do
    visit recipe_tag_relationships_url
    click_on "New Recipe Tag Relationship"

    click_on "Create Recipe tag relationship"

    assert_text "Recipe tag relationship was successfully created"
    click_on "Back"
  end

  test "updating a Recipe tag relationship" do
    visit recipe_tag_relationships_url
    click_on "Edit", match: :first

    click_on "Update Recipe tag relationship"

    assert_text "Recipe tag relationship was successfully updated"
    click_on "Back"
  end

  test "destroying a Recipe tag relationship" do
    visit recipe_tag_relationships_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Recipe tag relationship was successfully destroyed"
  end
end
