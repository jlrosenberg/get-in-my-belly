require 'test_helper'

class RecipeIngredientRelationshipsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @recipe_ingredient_relationship = recipe_ingredient_relationships(:one)
  end

  test "should get index" do
    get recipe_ingredient_relationships_url
    assert_response :success
  end

  test "should get new" do
    get new_recipe_ingredient_relationship_url
    assert_response :success
  end

  test "should create recipe_ingredient_relationship" do
    assert_difference('RecipeIngredientRelationship.count') do
      post recipe_ingredient_relationships_url, params: { recipe_ingredient_relationship: {  } }
    end

    assert_redirected_to recipe_ingredient_relationship_url(RecipeIngredientRelationship.last)
  end

  test "should show recipe_ingredient_relationship" do
    get recipe_ingredient_relationship_url(@recipe_ingredient_relationship)
    assert_response :success
  end

  test "should get edit" do
    get edit_recipe_ingredient_relationship_url(@recipe_ingredient_relationship)
    assert_response :success
  end

  test "should update recipe_ingredient_relationship" do
    patch recipe_ingredient_relationship_url(@recipe_ingredient_relationship), params: { recipe_ingredient_relationship: {  } }
    assert_redirected_to recipe_ingredient_relationship_url(@recipe_ingredient_relationship)
  end

  test "should destroy recipe_ingredient_relationship" do
    assert_difference('RecipeIngredientRelationship.count', -1) do
      delete recipe_ingredient_relationship_url(@recipe_ingredient_relationship)
    end

    assert_redirected_to recipe_ingredient_relationships_url
  end
end
