require 'test_helper'

class RecipeTagRelationshipsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @recipe_tag_relationship = recipe_tag_relationships(:one)
  end

  test "should get index" do
    get recipe_tag_relationships_url
    assert_response :success
  end

  test "should get new" do
    get new_recipe_tag_relationship_url
    assert_response :success
  end

  test "should create recipe_tag_relationship" do
    assert_difference('RecipeTagRelationship.count') do
      post recipe_tag_relationships_url, params: { recipe_tag_relationship: {  } }
    end

    assert_redirected_to recipe_tag_relationship_url(RecipeTagRelationship.last)
  end

  test "should show recipe_tag_relationship" do
    get recipe_tag_relationship_url(@recipe_tag_relationship)
    assert_response :success
  end

  test "should get edit" do
    get edit_recipe_tag_relationship_url(@recipe_tag_relationship)
    assert_response :success
  end

  test "should update recipe_tag_relationship" do
    patch recipe_tag_relationship_url(@recipe_tag_relationship), params: { recipe_tag_relationship: {  } }
    assert_redirected_to recipe_tag_relationship_url(@recipe_tag_relationship)
  end

  test "should destroy recipe_tag_relationship" do
    assert_difference('RecipeTagRelationship.count', -1) do
      delete recipe_tag_relationship_url(@recipe_tag_relationship)
    end

    assert_redirected_to recipe_tag_relationships_url
  end
end
