json.status "200"
json.recipes do 
  json.array! @recipes, partial: "recipes/recipe", as: :recipe

end