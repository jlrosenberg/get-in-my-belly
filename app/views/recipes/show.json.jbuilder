json.status "200"
json.recipe do
  json.partial! "recipes/recipe", recipe: @recipe
end
