json.extract! recipe, :id, :name, :description, :banner_image_url, :created_at, :updated_at, :author_id
json.author_name recipe.author_name
json.tags recipe.tags do |tag|
  json.partial! 'tags/tag', tag: tag
end
json.ingredients recipe.recipe_ingredient_relationships do |ingredient|
  json.ingredient_id ingredient.ingredient.id
  json.name ingredient.ingredient.name
  json.measurement_unit_id ingredient.measurement_unit_id
  json.quantity ingredient.quantity
end

json.steps recipe.recipe_steps do |step|
  json.id step.id
  json.description step.description
  json.order step.order
end
json.url recipe_url(recipe, format: :json)
