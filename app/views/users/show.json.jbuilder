json.status "200"
json.user do |json|
  # json.partial! 'users/user', user: current_user
  json.(@user, :id, :username, :first_name, :last_name, :profile_picture_url, :created_at)
end