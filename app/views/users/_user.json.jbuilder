json.status "200"
json.(user, :id, :email, :username, :first_name, :last_name, :profile_picture_url)
json.token @jwt