json.extract! recipe_ingredient_relationship, :id, :created_at, :updated_at
json.url recipe_ingredient_relationship_url(recipe_ingredient_relationship, format: :json)
