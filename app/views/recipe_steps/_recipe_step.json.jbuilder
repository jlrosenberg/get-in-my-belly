json.extract! recipe_step, :id, :created_at, :updated_at
json.url recipe_step_url(recipe_step, format: :json)
