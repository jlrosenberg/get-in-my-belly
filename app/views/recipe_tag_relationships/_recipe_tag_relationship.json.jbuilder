json.extract! recipe_tag_relationship, :id, :created_at, :updated_at
json.url recipe_tag_relationship_url(recipe_tag_relationship, format: :json)
