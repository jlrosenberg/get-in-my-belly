json.extract! measurement_unit, :id, :singular_name, :plural_name, :in_grams, :created_at, :updated_at
json.url measurement_unit_url(measurement_unit, format: :json)
