import * as React from 'react';
import {withStyles} from "@material-ui/core";
import {Theme} from '@material-ui/core'
import {CSSProperties} from "react";
 
type CSSPropertiesFunc = (props: any) => CSSProperties;
 
// This method should be called as a decorator and have a stylesheet class passed in, as in:
// @withStylesheet(Stylesheet)
// export class MyClass extends StyledComponent<Props, Stylesheet> {}
export function withStylesheet(stylesheet: any): (constructor: Function) => void {
 return withStyles((theme: Theme) => new stylesheet(theme), {withTheme: true});
}
 
export function withStylesheets(stylesheets: Array<any>): (constructor: Function) => void {
 const buildStylesheet = (theme: Theme) => {
   const combinedStylesheets = {};
   for (const stylesheet of stylesheets) {
     const newStylesheet = new stylesheet(theme);
     for (const key of Object.keys(newStylesheet)) {
       if (combinedStylesheets[key]) {
         console.error("@withStylesheets found duplicate rule '" + key + "'");
       }
       combinedStylesheets[key] = newStylesheet[key];
     }
   }
   return combinedStylesheets;
 };
 return withStyles(buildStylesheet, {withTheme: true});
}
 
// This is an identity function that just helps the Typescript compiler understand that it's contents are CSSProperties.
// It is intended to be used in the form:
// class Stylesheet {
//   outerDiv = rule({ ... });
// }
export function rule(css: CSSProperties | CSSPropertiesFunc): string {
 return css as any;
}
 
// This adds CSS type safety to nested CSS.  It is intended to be used with spread syntax as in:
// rule({
//   height: 20,
//   ...subRule({ ... })
// })
export function subRule(selector: string, css: CSSProperties | CSSPropertiesFunc) : {[index: string]: CSSProperties} {
 return {
   [selector]: css as any
 };
}
 
// The StyledComponent is a very simple parent class that adds an accessor that returns this.props.classes cast
// as the Stylesheet for the component.
export class StyledComponent<Props, Stylesheet, State = {}> extends React.Component<Props, State> {
 get classes(): Stylesheet {
   return (this.props as any).classes;
 }
}
 
// The StyledPureComponent is a very simple parent class that adds an accessor that returns this.props.classes cast
// as the Stylesheet for the component.
export class StyledPureComponent<Props, Stylesheet, State = {}> extends React.PureComponent<Props, State> {
 get classes(): Stylesheet {
   return (this.props as any).classes;
 }
}
