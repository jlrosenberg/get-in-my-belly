import { Recipe } from "../models/Recipe"
import { API } from "../api/Api"
const camelcaseKeys = require('camelcase-keys');

export class RecipeStore{
  private static instance:RecipeStore
  private recipes: Array<Recipe>
  private recipesById: Map<number, Recipe>
  private recipesByTagId: Map<number, Array<Recipe>>

  private constructor(){
    RecipeStore.instance = this
    this.recipes = []
    this.recipesById = new Map()
    this.recipesByTagId = new Map()
  }

  static getInstance(){
    if(RecipeStore.instance == null){
      RecipeStore.instance = new RecipeStore()
    }

    return RecipeStore.instance
  }

  getRecipeById(id:number){
    // console.log(id)
    // console.log("CURRENT")
    // console.log(this.recipesById)
    // console.log(this.recipesById.get(id))
    // debugger
    return this.recipesById.get(id)
  }

  getRecipesByTagId(tagId: number):Array<Recipe>|undefined{
    return this.recipesByTagId.get(tagId);
  }

  getRecipes(){
    return this.recipes
  }

  addRecipe(){
// TODO for creating new recipe and adding it.
  }

  async loadRecipe(id: number){
    let response = await API.getRecipe(id)
    if(response.status == "200"){
      // Parse into recipe model
      let r = camelcaseKeys(response.recipe, {deep: true}) as any
      let recipe = Recipe.fromJson(r)
      this.recipes.push(recipe)
      this.recipesById.set(recipe.id, recipe)
      console.log(this.recipesById)
      recipe.tags.forEach((tag)=>{
        if(this.recipesByTagId.has(tag.id)){
          (this.recipesByTagId.get(tag.id) as Array<Recipe>).push(recipe)
        }else{
          this.recipesByTagId.set(tag.id, []);
          (this.recipesByTagId.get(tag.id) as Array<Recipe>).push(recipe)
        }
      })
      // console.log(response.recipe) 
    }else{
      // TODO
    }

  }

  async loadRecipes(){
    let response = await API.getRecipes();
    if(response.status == "200"){
      response.recipes.forEach((rec)=>{
        let r = camelcaseKeys(rec, {deep: true}) as any
        let recipe = Recipe.fromJson(r)
        this.recipes.push(recipe)
        this.recipesById.set(recipe.id, recipe)
        console.log(this.recipesById)
        recipe.tags.forEach((tag)=>{
          if(this.recipesByTagId.has(tag.id)){
            (this.recipesByTagId.get(tag.id) as Array<Recipe>).push(recipe)
          }else{
            this.recipesByTagId.set(tag.id, []);
            (this.recipesByTagId.get(tag.id) as Array<Recipe>).push(recipe)
          }
        })
      })

    }else{
      console.log("failed to retrieve recipes")
    }
  }
  
}