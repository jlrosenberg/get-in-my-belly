import { API } from "../api/Api"
import { Tag } from "../models/Tag";
const camelcaseKeys = require('camelcase-keys');

export class TagStore{
  private static instance:TagStore
  private tags: Array<Tag>
  private tagsById: Map<number, Tag>

  private constructor(){
    TagStore.instance = this
    this.tags = []
    this.tagsById = new Map()
  }

  static getInstance(){
    if(TagStore.instance == null){
      TagStore.instance = new TagStore()
    }

    return TagStore.instance
  }

  getTags(){
    return this.tags
  }

  addTag(name: string, id: number){
    let tag = {
      id: id,
      name: name
    }

    this.tags.push(tag)
    this.tagsById.set(id, tag)
  }

  getTagById(id: any){
    let parsed = parseInt(id) //Handle the js bug with weird type crap
    return this.tagsById.get(parsed)
  }

  async loadTags(){
    let response = await API.getTags();
    console.log(response)
    if(response.status == "200"){
      response.tags.forEach((m)=>{
        let mu = camelcaseKeys(m, {deep: true}) as any
        let tag = {
          id: mu.id,
          name: mu.name
        }
        this.tags.push(tag)
        this.tagsById.set(tag.id, tag)
      })
    }else{
      // TODO handle the error case
    }
  }
}