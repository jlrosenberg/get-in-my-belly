import { API } from "../api/Api"
import { Ingredient } from "../models/Ingredient";
const camelcaseKeys = require('camelcase-keys');

export class IngredientStore{
  private static instance:IngredientStore
  private ingredients: Array<Ingredient>
  private ingredientsById: Map<number, Ingredient>

  private constructor(){
    IngredientStore.instance = this
    this.ingredients = []
    this.ingredientsById = new Map()
  }

  static getInstance(){
    if(IngredientStore.instance == null){
      IngredientStore.instance = new IngredientStore()
    }

    return IngredientStore.instance
  }

  getIngredients(){
    return this.ingredients
  }

  getIngredientById(id: any){
    let parsed = parseInt(id) //Handle the js bug with weird type crap
    return this.ingredientsById.get(parsed)
  }

  async loadIngredients(){
    let response = await API.getIngredients();
    console.log(response)
    if(response.status == "200"){
      response.measurement_units.forEach((m)=>{
        let mu = camelcaseKeys(m, {deep: true}) as any
        let ingredient = {
          id: mu.id,
          name: mu.name,
          quantity: mu.quantity,
          measurementUnitId: mu.measurementUnitId
        }
        this.ingredients.push(ingredient)
        this.ingredientsById.set(ingredient.id, ingredient)
      })
    }else{
      // TODO handle the error case
    }
  }
}