import { Recipe } from "../models/Recipe"
import { API } from "../api/Api"
import { MeasurementUnit } from "../models/MeasurementUnit";
const camelcaseKeys = require('camelcase-keys');

export class MeasurementUnitStore{
  private static instance:MeasurementUnitStore
  private measurementUnits: Array<MeasurementUnit>
  private measurementUnitsById: Map<number, MeasurementUnit>

  private constructor(){
    MeasurementUnitStore.instance = this
    this.measurementUnits = []
    this.measurementUnitsById = new Map()
  }

  static getInstance(){
    if(MeasurementUnitStore.instance == null){
      MeasurementUnitStore.instance = new MeasurementUnitStore()
    }

    return MeasurementUnitStore.instance
  }

  getMeasurementUnits(){
    return this.measurementUnits
  }

  getMeasurementUnitById(id: any){
    let parsed = parseInt(id) //Handle the js bug with weird type crap
    return this.measurementUnitsById.get(parsed)
  }

  async loadMeasurementUnits(){
    let response = await API.getMeasurementUnits();
    console.log(response)
    if(response.status == "200"){
      response.measurement_units.forEach((m)=>{
        let mu = camelcaseKeys(m, {deep: true}) as any
        let measurementUnit = MeasurementUnit.fromJson(mu)
        this.measurementUnits.push(measurementUnit)
        this.measurementUnitsById.set(measurementUnit.id, measurementUnit)
      })
    }else{
      // TODO handle the error case
    }
  }
}