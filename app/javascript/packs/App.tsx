// Run this example by adding <%= javascript_pack_tag 'hello_react' %> to the head of your layout file,
// like app/views/layouts/application.html.erb. All it does is render <div>Hello React</div> at the bottom
// of the page.

import React from 'react'
import ReactDOM from 'react-dom'
import {MainComponent} from '../MainComponent'

console.log("ADDING EVENT LISTENER")

window.onload = function(){
  // code here
  console.log("Firing")
  ReactDOM.render(
    <MainComponent/>,
    
    document.getElementById("topdiv")
  )
};

window.addEventListener('load', (event) => {
  console.log('page is fully loaded');
},{capture:true});

document.addEventListener('DOMContentLoaded', () => {
  console.log("OLD FIRING")
  ReactDOM.render(
    <MainComponent/>,
    
    document.getElementById("topdiv")
  )
  // var parent = document.getElementById("topdiv");


})
