import {StyledComponent, withStylesheet} from '../StyledComponent'
import React from 'react';
import { Ingredient, RecipeStep } from '../models/Recipe';
import { MeasurementUnitStore } from '../stores/MeasurementUnitStore';
import { TagStore } from '../stores/TagStore';
import { IngredientStore } from '../stores/IngredientStore';
import { Typography } from '@material-ui/core';


interface Props{

}

class Stylesheet{

}



interface RecipeIngredient{
  ingredientId: number
  quantity: number
  measurementUnitId: number
}

interface State{
  formState: RecipeFormData
  newIngredientModalOpen: boolean
  newTagModalOpen: boolean
}

interface RecipeFormData{
  ingredients: Array<RecipeIngredient>
  steps: Array<RecipeStep>
  tags: Array<number>
  name: string
  description: string
  bannerImageUrl: string
}


@withStylesheet(Stylesheet)
export class AddRecipePage extends StyledComponent<Props, Stylesheet,State>{

  constructor(props){
    super(props)
    this.state={
      formState: {
        ingredients:[],
        steps:[],
        tags:[],
        name:"",
        description:"",
        bannerImageUrl:""
      },
      newIngredientModalOpen: false,
      newTagModalOpen: false
    }

    this.loadMeasurementUnits()
    this.loadTags()
    this.loadIngredients()
  }


  async loadIngredients(){
    await IngredientStore.getInstance().loadIngredients()
  }

  async loadTags(){
    await TagStore.getInstance().loadTags()
  }

  async loadMeasurementUnits(){
    await MeasurementUnitStore.getInstance().loadMeasurementUnits()
  }

  render(){
    return(
      <div>
        <Typography variant="h3">Add new recipe</Typography>
      </div>
    );
  }

}