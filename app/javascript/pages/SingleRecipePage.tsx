import { StyledComponent, withStylesheet, rule } from "../StyledComponent";
import React from "react";
import { RecipeStore } from "../stores/RecipeStore";
import { Recipe } from "../models/Recipe";
import { LoadingWheel } from "../components/LoadingWheel";
import { Typography, Button, Link, Chip, Box } from "@material-ui/core";
import { Tag } from "../models/Tag";
import { MeasurementUnit } from "../models/MeasurementUnit";
import { MeasurementUnitStore } from "../stores/MeasurementUnitStore";

interface Props{
  id: number
}

class Stylesheet{
  center = rule({
    display: "flex",
    marginTop: 150,
    alignContent: "center",
    justifyContent: "center"
  })

  root = rule({
    padding: 16
  })

  bannerImage = rule({
    width: '100%',
    maxHeight: 300
  })

  authorName = rule({
    paddingRight: 8
  })
}

interface State{
  recipe: Recipe | undefined
}


@withStylesheet(Stylesheet)
export class SingleRecipePage extends StyledComponent<Props,Stylesheet, State>{
  constructor(props){
    super(props)
    this.state = {
      recipe: undefined
    }

    this.loadMeasurementUnits()
    this.loadRecipe()
  }
  componentDidMount(){
    
  }

  async loadMeasurementUnits(){
    await MeasurementUnitStore.getInstance().loadMeasurementUnits()
    this.forceUpdate()
  }

  async loadRecipe(){
    await RecipeStore.getInstance().loadRecipe(parseInt(this.props.id.toString()))
    let recipe = RecipeStore.getInstance().getRecipeById(parseInt(this.props.id.toString()))
    this.setState({recipe: recipe})
  }
  renderLoadingCircle(){
    return(
      <div className={this.classes.center}>
                {this.renderBackToRecipeBrowserButton()}   

        <LoadingWheel/>
      </div>
    );
  }

  renderBackToRecipeBrowserButton(){
    return(
      <a href="http://localhost:3000" onClick={() => {window.location.assign("http://localhost:3000"); }}>Go back to all recipes</a>
    );
  }

  redirectToRecipesByTagId = (tag: Tag) => {

  }

  getMeasurementString(quantity: number, measurementUnitId: number){
    let measurementUnit = MeasurementUnitStore.getInstance().getMeasurementUnitById(measurementUnitId)
    if(measurementUnit == undefined){
      return quantity
    }
    if(quantity>1){
      return quantity.toString() + " " + measurementUnit.pluralName
    }else{
      return quantity.toString() + " " + measurementUnit.singularName
    }
  }

  renderRecipeHeader(){
    let recipe = this.state.recipe as Recipe
    return(
      <div>       
        {this.renderBackToRecipeBrowserButton()}   

        <Typography variant="h4">{recipe.name}</Typography>
        <Typography variant="caption" className={this.classes.authorName}>posted by {recipe.authorName}</Typography>
        {this.renderRecipeTagChips()}
        {recipe.bannerImageUrl && <img src={recipe.bannerImageUrl} className={this.classes.bannerImage}/>}
        <Typography variant="body1">{recipe.description}</Typography>
          
      </div>
    );
  }

  renderRecipeTagChips(){
    let recipe = this.state.recipe as Recipe
    return recipe.tags.map(tag => this.renderTagChip(tag))
  }

  renderTagChip(tag: Tag): any{

    return <Chip label={tag.name} onClick={() => {this.redirectToRecipesByTagId(tag)}}/>
  
  }

  renderIngredients(){
    let recipe = this.state.recipe as Recipe
    let ingredientSet = recipe.ingredients

    return <div id="ingredients">
      <br />
      <Typography variant="h6">Ingredients:</Typography>
      <ul>
      {ingredientSet.map(value => 
        <li>{this.getMeasurementString(value.quantity, value.measurementUnitId)} {value.name}</li>)}
      </ul>
    </div>

  }

  renderSteps(){
    let recipe = this.state.recipe as Recipe
    let stepsSet = recipe.steps

    return <div id="steps">
      <br />
      <Typography variant="h6">How to Make:</Typography>
      <ul>
      {stepsSet.map(value => 
        <li>{value.description}</li>)}
      </ul>
    </div>
  }

  render(){
    let recipe = this.state.recipe
    if(recipe == undefined){
      return this.renderLoadingCircle()
    }else{
      return(
        <div className={this.classes.root}>
          {this.renderRecipeHeader()}
          {this.renderIngredients()}
          {this.renderSteps()}
        </div>
      );
    }

  }




}