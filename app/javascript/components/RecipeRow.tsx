import { StyledComponent } from "../StyledComponent";
import { Recipe } from "../models/Recipe";
import React from "react";
import { Typography, ListItem, ListItemText, ListItemSecondaryAction, Chip } from "@material-ui/core";
import { RecipeHeader } from "./RecipeHeader";
import { Tag } from "../models/Tag";

interface Props{
  recipe: Recipe
}

class Stylesheet{

}

interface State{

}

function ListItemLink(props) {
  return <ListItem button component="a" {...props} />;
}

export class RecipeRow extends StyledComponent<Props, Stylesheet, State>{

  redirectToRecipesByTagId = (tag) =>{

  }


  renderRecipeChips(){
    let recipe = this.props.recipe as Recipe
    // console.log(recipe.tags.map(tag => this.renderChip(tag)))
    return recipe.tags.map(tag => this.renderChip(tag))
  }

  renderChip(tag: Tag): any{
    return <Chip label={tag.name} onClick={() => {this.redirectToRecipesByTagId(tag)}}/>
  }
  
  render(){
    let recipe = this.props.recipe
    return(
      <div>
        <ListItemLink  href={`http://localhost:3000/recipes/${recipe.id}`}>
          <ListItemText primary={recipe.name} secondary={recipe.description}></ListItemText>
          <ListItemSecondaryAction>
            {this.renderRecipeChips()}
          </ListItemSecondaryAction>
        </ListItemLink>

      </div>
    );
  }
}