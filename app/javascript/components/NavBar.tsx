import { StyledComponent, rule, withStylesheet } from "../StyledComponent";
import { Toolbar, IconButton, AppBar, Typography, Button, Snackbar, Divider} from "@material-ui/core";
import MenuIcon from '@material-ui/icons/Menu';
import RestaurantIcon from '@material-ui/icons/Restaurant';
import React from "react";
import { API } from "../api/Api";
import { LogInModal } from "./LogInModal";
import { RegistrationModal } from "./RegistrationModal";


interface Props{

}

class Stylesheet{
  root = rule({
    flexGrow: 1
  })

  menuButton = rule({
    marginRight: 8,
  })

  title = rule({
    flexGrow: 1
  })

  divider= rule({
    height: 50,
    color: "white"
  })
}

interface State{
  logInModalOpen: boolean
  registrationModalOpen: boolean
  loggedIn: boolean
  message: string | null
}

@withStylesheet(Stylesheet)
export class NavBar extends StyledComponent<Props, Stylesheet, State>{
  constructor(props){
    super(props);
    this.state ={
      loggedIn: false,
      logInModalOpen: false,
      registrationModalOpen: false,
      message: null
    }
  }
  onLoginClick = () => {
    this.setState({logInModalOpen: true})
  }

  onCloseLoginModalClick = () => {
    this.setState({logInModalOpen: false})
  }

  onCloseRegistrationModalClick = () => {
    this.setState({registrationModalOpen: false})
  }

  onRegisterClick = () => {
    this.setState({registrationModalOpen: true})
  }

  onLoginSuccess = () => {
    this.setState({message: "Logged in successfully", logInModalOpen: false})
  }
  onRegistrationSuccess = () => {
    this.setState({message: "Registered successfully", registrationModalOpen: false})
  }

  onTagsClick = () => {
    window.location.href="http://localhost:3000/tags"
  }

  onRecipesClick = () => {
    window.location.href="http://localhost:3000/"
  }

  onUsersClick = () => {
    window.location.href="http://localhost:3000/users"
  }

  async req(){
    let result = await API.getUser(1);
    console.log(result)
  }

  async login(){
    let result = await API.login("joshua@rosenbergs.org", "password!")
    console.log(result)
  }


  render(){
    return(
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" className={this.classes.menuButton} color="inherit" aria-label="menu">
            <RestaurantIcon />
          </IconButton>
          <Typography variant="h6" className={this.classes.title}>
            Get In My Belly
          </Typography>
          <Button color="inherit" onClick={this.onRecipesClick}>Recipes</Button>   
          <Button color="inherit" onClick={this.onTagsClick}>Tags</Button>   
          <Button color="inherit" onClick={this.onUsersClick}>Users</Button>   
          <Divider orientation="vertical" className={this.classes.divider}/> 
            {!this.state.loggedIn && <Button color="inherit" onClick={this.onLoginClick}>Login</Button>}
          <Button color="inherit" onClick={this.onRegisterClick}>Register</Button>
        </Toolbar>

        {this.state.logInModalOpen && <LogInModal onClose={this.onCloseLoginModalClick} onLoginSuccess={this.onLoginSuccess}/>}
        {this.state.registrationModalOpen && <RegistrationModal onClose={this.onCloseRegistrationModalClick} onRegistrationSuccess={this.onRegistrationSuccess}/>}
        

        {this.state.message && 
          <Snackbar
            open
            onClose={()=>{this.setState({message: null})}}
            autoHideDuration={3000}
            // className={this.classes.errorToast}
            message={this.state.message}
            anchorOrigin={{
              vertical: 'top',
              horizontal: 'center'
            }}
          />
        }
      </AppBar>
    );
  }

}