import { StyledComponent, withStylesheet, rule } from "../StyledComponent";
import React from "react";
import { RecipeStore } from "../stores/RecipeStore";
import { Recipe } from "../models/Recipe";
import { LoadingWheel } from "./LoadingWheel";
import { Table, TableHead, TableRow, TableCell, TableBody, List, Paper, Card } from "@material-ui/core";
import { RecipeRow } from "./RecipeRow";

interface Props{

}


class Stylesheet{
  center = rule({
    display: "flex",
    marginTop: 150,
    alignContent: "center",
    justifyContent: "center"
  })
  
  card = rule({
    margin: 16
  })
}

interface State{
  recipes: Array<Recipe> | undefined
}
@withStylesheet(Stylesheet)
export class RecipeBrowser extends StyledComponent<Props, Stylesheet, State>{
  constructor(props){
    super(props)
    this.state = {
      recipes: undefined
    }
    this.loadRecipes();
  }

  async loadRecipes(){
    await RecipeStore.getInstance().loadRecipes();
    let recipes = RecipeStore.getInstance().getRecipes()

    this.setState({recipes: recipes})

  }

  renderLoadingCircle(){
    return(
      <div className={this.classes.center}>
        <LoadingWheel/>
      </div>
    );
  }

  renderRow(recipe: Recipe){
    return(
      <RecipeRow recipe={recipe}></RecipeRow>
    );
  }

  render(){
    if (this.state.recipes == undefined){
      return this.renderLoadingCircle()
    }

    return(
        <Card className={this.classes.card}>
        <div>
          <List>
            {this.state.recipes.map(recipe => this.renderRow(recipe))}
          </List> 
        </div>
        </Card>




    );

  }
}