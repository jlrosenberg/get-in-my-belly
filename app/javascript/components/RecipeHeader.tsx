import {Recipe} from '../models/Recipe'
import { StyledComponent, withStylesheet } from "../StyledComponent";
import React from 'react';

interface Props{
  recipe: Recipe
}

class Stylesheet{

}

@withStylesheet(Stylesheet)
export class RecipeHeader extends StyledComponent<Props, Stylesheet>{
  render(){
    return (
      <div></div>
    );


  }

}