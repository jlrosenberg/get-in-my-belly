import { StyledComponent, withStylesheet } from "../StyledComponent";
import React from "react";
import { Modal, TextField, Button, Typography } from "@material-ui/core";
import { API } from "../api/Api";
import { TagStore } from "../stores/TagStore";

interface Props{
  onSubmit(): void
  onClose(): void
}

class Stylesheet{

}

interface State{
  tag: string
  error?: string
}

@withStylesheet(Stylesheet)
export class NewIngredientModal extends StyledComponent<Props, Stylesheet, State>{
  constructor(props){
    super(props)
    this.state={
      tag: ""
    }
  }

  onClose = () =>{
    this.props.onClose()
  }

  onSubmit = () => {

  }

  async postTag(){
    let tag = this.state.tag
    let response = await API.postTag(tag)

    if(response.status == "422"){
      this.setState({error: response.error})
    }else{
      TagStore.getInstance().addTag(response.name, response.id)
    }
  }

  render(){
    return(
      <Modal
        open
        onClose={this.onClose}
      >
        <div>
          <Typography>Create new Tag</Typography>
          <TextField
            autoFocus
            id="tag"
            label="Tag"
            required
            value={this.state.tag}
            onChange={(e) => {this.setState({tag: e.target.value})}}
            fullWidth
          />
          <Button onSubmit={this.onSubmit}>
            Create
          </Button>
        </div>
      </Modal>
    );
  }

}