import { StyledComponent, withStylesheet, rule } from "../StyledComponent";
import React from "react";
import { makeStyles} from "@material-ui/styles";

interface Props{

}

class Stylesheet{





}
const styles = makeStyles(theme => ({
  '@keyframes rotation':{
    from: {
      transform: 'rotate(0deg)',
    },
    to: {
      transform: "rotate(359deg)"
    }
  },

  root:{
    animation: "$rotation 2s infinite linear",
    width: 100,
    height: 100,
    display: 'flex',
    
  }
}));




export function LoadingWheel(){
  const classes = styles();
  
  return(
    <img src="https://www.stickpng.com/assets/images/5888ca78bc2fc2ef3a1860da.png" className={classes.root}></img>
  
  );
}