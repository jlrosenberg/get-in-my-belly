import { StyledComponent, withStylesheet, rule, subRule } from "../StyledComponent";
import { Dialog, DialogTitle, DialogContent, DialogContentText, TextField, DialogActions, Button, Typography, Snackbar } from "@material-ui/core";
import React from "react";
import { API } from "../api/Api";
import { Colors } from "../Theme";
import { LoadingWheel } from "./LoadingWheel";
interface Props{
  onClose(): void
  onRegistrationSuccess(): void


}

class Stylesheet{
  errorToast = rule({
    ...subRule('& .MuiSnackbarContent-root', {
      backgroundColor: Colors.error,
    })
  })

  loading = rule({
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  })

  root = rule({

  })

}

interface State{
  email: string
  password: string
  firstName: string
  lastName: string
  profilePictureUrl?: string
  registrationPending: boolean
  error?: string | null
  errors?: Map<string, string>
  username: string

}

@withStylesheet(Stylesheet)
export class RegistrationModal extends StyledComponent<Props, Stylesheet, State>{
  constructor(props){
    super(props);
    this.state ={
      email: "",
      password: "",
      registrationPending: false,
      firstName: "",
      lastName: "",
      username: ""
    }
  }
  
  onRegisterClick = () => {
  
      this.register();
  

  }

  // hasError(key: string): boolean{
  //   return this.state.errors.has(key);
  // }

  onEmailChange = (event: any) => {
    this.setState({email: event.target.value})
  }

  onPasswordChange = (event: any) =>{
    this.setState({password: event.target.value})

  }

  async register(){
    this.setState({registrationPending: true})
    let response = await API.register(this.state.email, this.state.username, this.state.password, this.state.firstName, this.state.lastName, this.state.profilePictureUrl)
    if(response.status == "200"){
      this.props.onRegistrationSuccess()
    }else{
      console.log(response);
      this.setState({error:"Registration failed.... please try again", registrationPending: false})
      
    }
  }

  render(){
    return(
      <Dialog
        open
        onClose = {this.props.onClose}
        aria-labelledby="form-dialog-title"
        className = {this.classes.root}
      >
        <DialogTitle id="form-dialog-title">Register</DialogTitle>

          <div>
            <DialogContent>
              <DialogContentText>
                To register for this website, please fill out the form below
              </DialogContentText>
              {this.state.error && 
                <Snackbar
                  open
                  onClose={()=>{this.setState({error: undefined})}}
                  autoHideDuration={10000}
                  className={this.classes.errorToast}
                  message={this.state.error}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center'
                  }}
                />
              }
              {this.state.registrationPending ?
                <div className={this.classes.loading}>
                  <LoadingWheel/><br/>
                  Logging in ...
                </div>
              :
              <div>
                <TextField
                  autoFocus
                  id="email"
                  label="Email Address"
                  required
                  type="email"
                  value={this.state.email}
                  onChange={this.onEmailChange}
                  fullWidth
                />
                            
                <TextField
                  autoFocus
                  id="username"
                  label="Username"
                  required
                  value={this.state.username}
                  onChange={(e) => {this.setState({username: e.target.value})}}
                  fullWidth
                />
                <TextField
                  id="password"
                  required
                  label="Password"
                  value={this.state.password}
                  type="password"
                  onChange={this.onPasswordChange}
                  fullWidth
                />
                <TextField
                  autoFocus
                  id="firstName"
                  label="First Name"
                  required
                  value={this.state.firstName}
                  onChange={(e) => {this.setState({firstName: e.target.value})}}
                  fullWidth
                />
                <TextField
                  autoFocus
                  id="lastName"
                  label="Last Name"
                  required
                  value={this.state.lastName}
                  onChange={(e) => {this.setState({lastName: e.target.value})}}
                  fullWidth
                />
                <TextField
                  autoFocus
                  id="profilePictureUrl"
                  label="Profile Picture Url"
                  value={this.state.profilePictureUrl}
                  onChange={(e) => {this.setState({profilePictureUrl: e.target.value})}}
                  fullWidth
                />

              </div>
            }
            </DialogContent>
            <DialogActions>
              <Button onClick={this.props.onClose} color="secondary">
                Cancel
              </Button>
              <Button onClick={this.onRegisterClick} color="primary">
                Register
              </Button>
            </DialogActions>
          </div>
        

      </Dialog>


     

    );
  }
}