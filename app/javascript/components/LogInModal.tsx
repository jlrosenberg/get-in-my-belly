import { StyledComponent, withStylesheet, rule, subRule } from "../StyledComponent";
import { Dialog, DialogTitle, DialogContent, DialogContentText, TextField, DialogActions, Button, Typography, Snackbar } from "@material-ui/core";
import React from "react";
import { API } from "../api/Api";
import { Colors } from "../Theme";
import { LoadingWheel } from "./LoadingWheel";
interface Props{
  onClose(): void
  onLoginSuccess(): void


}

class Stylesheet{
  errorToast = rule({
    ...subRule('& .MuiSnackbarContent-root', {
      backgroundColor: Colors.error,
    })
  })

  loading = rule({
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  })

  root = rule({

  })

}

interface State{
  email: string
  password: string
  loggingIn: boolean
  error?: string | null
  emailError?: boolean
  passwordError?: boolean
}

@withStylesheet(Stylesheet)
export class LogInModal extends StyledComponent<Props, Stylesheet, State>{
  constructor(props){
    super(props);
    this.state ={
      email: "",
      password: "",
      loggingIn: false,
    }
  }
  
  onLoginClick = () => {
    if(this.state.email == ""){
      this.setState({emailError: true, error: "Please fill out all required fields"})
    }else{
      this.setState({emailError: false})
    }
    if(this.state.password == ""){
      this.setState({passwordError: true, error: "Please fill out all required fields"})
    }else{
      this.setState({passwordError: false})
    }

    if(!this.state.emailError && !this.state.passwordError){
      this.setState({error: null})
      this.login()
    }

  }

  onEmailChange = (event: any) => {
    this.setState({email: event.target.value})
  }

  onPasswordChange = (event: any) =>{
    this.setState({password: event.target.value})

  }

  async login(){
    this.setState({loggingIn: true})
    let response = await API.login(this.state.email, this.state.password)
    if(response.status == "200"){
      this.props.onLoginSuccess()
    }else{
      console.log(response);
      this.setState({error:"Login attempt failed. Please make sure your email and password are spelled correctly and try again.", loggingIn: false})
      
    }
  }

  render(){
    return(
      <Dialog
        open
        onClose = {this.props.onClose}
        aria-labelledby="form-dialog-title"
        className = {this.classes.root}
      >
        <DialogTitle id="form-dialog-title">Login</DialogTitle>

          <div>
            <DialogContent>
              <DialogContentText>
                To log in to this website, please enter your email address and password below.
              </DialogContentText>
              {this.state.error && 
                <Snackbar
                  open
                  onClose={()=>{this.setState({error: undefined})}}
                  autoHideDuration={10000}
                  className={this.classes.errorToast}
                  message={this.state.error}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center'
                  }}
                />
              }
              {this.state.loggingIn ?
                <div className={this.classes.loading}>
                  <LoadingWheel/><br/>
                  Logging in ...
                </div>
              :
              <div>
                <TextField
                  autoFocus
                  id="name"
                  label="Email Address"
                  required
                  type="email"
                  value={this.state.email}
                  error={this.state.emailError}
                  onChange={this.onEmailChange}
                  fullWidth
                />
                <TextField
                  id="password"
                  required
                  label="Password"
                  value={this.state.password}
                  error={this.state.passwordError}
                  type="password"
                  onChange={this.onPasswordChange}
                  fullWidth
                />
              </div>
            }
            </DialogContent>
            <DialogActions>
              <Button onClick={this.props.onClose} color="secondary">
                Cancel
              </Button>
              <Button onClick={this.onLoginClick} color="primary">
                Login
              </Button>
            </DialogActions>
          </div>
        

      </Dialog>


     

    );
  }
}