
const baseUrl = "http://localhost:3000"

export class API{

  static async login(email: string, password: string) {
    let body ={
      "user":{
        "email": email,
        "password": password
      }
    }

    const response = await fetch(`${baseUrl}/api/v1/users/login`, {
      method: 'POST',
      credentials: 'include',
      body: JSON.stringify(body), 
      headers: {
        "Content-Type": "application/json"
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
    });
    return await response.json(); 
  }

  static async register(email: string, username: string, password: string, firstName: string, lastName: string, profilePictureUrl?: string){
    let body ={
      "user":{
        "email": email,
        "password": password,
        "username": username,
        "first_name": firstName,
        "last_name": lastName,
        "profile_picture_url": profilePictureUrl ? profilePictureUrl : "https://icon-library.net/images/default-user-icon/default-user-icon-4.jpg"
      }
    }
    const response = await fetch(`${baseUrl}/api/v1/users`, {
      method: 'POST',
      credentials: 'include',
      body: JSON.stringify(body), 
      headers: {
        "Content-Type": "application/json"
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
    });
    return await response.json(); 
  }

  static async getUser(userId: number){
    let response = await fetch(`${baseUrl}/api/v1/users/${userId}`, {
      credentials: 'include',
      headers: {
        "Content-Type": "application/json"
      },
    })

    return await response.json();
  }

  static async getRecipe(recipeId: number){
    let response = await fetch(`${baseUrl}/api/v1/recipes/${recipeId}`, {
      credentials: 'include',
      headers: {
        "Content-Type": "application/json"
      },
    })

    return await response.json();
  }

  static async getRecipes(){
    let response = await fetch(`${baseUrl}/api/v1/recipes`, {
      credentials: 'include',
      headers: {
        "Content-Type": "application/json"
      },
    })

    return await response.json();
  }

  static async getMeasurementUnits(){
    let response = await fetch(`${baseUrl}/api/v1/measurement_units`, {
      credentials: 'include',
      headers: {
        "Content-Type": "application/json"
      },
      
    })

    return await response.json();
  }

  static async getTags(){
    let response = await fetch(`${baseUrl}/api/v1/tags`, {
      credentials: 'include',
      headers: {
        "Content-Type": "application/json"
      },
    })

    return await response.json();
  }
  static async getIngredients(){
    let response = await fetch(`${baseUrl}/api/v1/ingredients`, {
      credentials: 'include',
      headers: {
        "Content-Type": "application/json"
      },
    })

    return await response.json();
  }

  static async postTag(tag: string){
    let body = {
      "tag":{
        "name": tag
      }
    }
    let response = await fetch(`${baseUrl}/api/v1/tags`, {
      credentials: 'include',
      method: 'POST',
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(body), 
    })

    return await response.json();
  }
}