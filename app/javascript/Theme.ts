import { createMuiTheme } from '@material-ui/core/styles';

export const Colors = {
  primary: "#2885bf",
  error: "#ab2f20"
}
export const Theme = createMuiTheme({
  palette: {
    primary: {
      main: '#2885bf',
    },
  },
});