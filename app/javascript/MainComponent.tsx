
import React from 'react'
import { Button, Typography} from '@material-ui/core'
import { StyledComponent, withStylesheet, rule } from './StyledComponent';
import { ThemeProvider } from '@material-ui/core/styles';
import { Theme} from './Theme'
import { NavBar } from './components/NavBar';
import { LoadingWheel } from './components/LoadingWheel';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'
import { RecipeBrowser } from './components/RecipeBrowser';
import { SingleRecipePage } from './pages/SingleRecipePage';



interface Props{

}

class Stylesheet{
  root = rule({

  })

  center = rule({
    display: "flex",
    marginTop: 150,
    alignContent: "center",
    justifyContent: "center"
  })

  center2 = rule({
    display: "flex",
    marginTop: 20,
    alignContent: "center",
    justifyContent: "center"
  })

}

@withStylesheet(Stylesheet)
export class MainComponent extends StyledComponent<Props, Stylesheet>{

  render(){
    return(
      <ThemeProvider theme={Theme}>
        <NavBar/>
        <Router>
          <Route exact path="/" component={RecipeBrowser}></Route>
          <Route exact path="/recipes/:id" render={(props:any) => (
            <SingleRecipePage id={props.match.params.id} />
          )}   />

        </Router>
        {/* <div className={this.classes.center}>
      
          <LoadingWheel></LoadingWheel>
      
    
        

        </div>
        <div className={this.classes.center2}>
        <Typography variant="h3">Coming soon...</Typography>
        </div> */}

      </ThemeProvider>

    );
  }
}