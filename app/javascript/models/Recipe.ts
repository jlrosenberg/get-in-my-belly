import { Tag } from "./Tag"

export interface RecipeStep{
  order: number
  description: string
  id?: number
}

export interface Ingredient{
  id: number
  quantity: number
  name: string
  measurementUnitId: number
}

export class Recipe{
  authorId: number
  authorName: string
  bannerImageUrl: string
  createdAt: Date
  description: string
  id: number
  ingredients: Array<Ingredient> //TODO change this once ingredient model is created 
  name: string
  steps: Array<RecipeStep>
  tags: Array<Tag>
  url: string

  constructor(authorId: number, authorName: string, createdAt: Date, bannerImageUrl: string, description: string, id: number, ingredients: Array<Ingredient>, name: string, steps: Array<RecipeStep>, tags: Array<Tag>, url:string){
    this.authorId = authorId
    this.authorName = authorName
    this.bannerImageUrl = bannerImageUrl
    this.createdAt = createdAt
    this.description = description
    this.id = id
    this.ingredients = ingredients
    this.name = name
    this.steps = steps
    this.tags = tags
    this.url = url
  }

  static fromJson(json): Recipe{
    let recipe = new Recipe(json.authorId, json.authorName, json.createdAt, json.bannerImageUrl, json.description, json.id, json.ingredients, json.name, json.steps, json.tags, json.url);

    return recipe

  }

  update(){
    // TODO - this needs to PATCH or PUT an update to the recipe back to the server. 
  }
}
