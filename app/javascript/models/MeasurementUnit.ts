export class MeasurementUnit{
  singularName: string
  pluralName: string
  inGrams: number
  id: number

  constructor(singularName, pluralName, inGrams, id){
    this.singularName = singularName
    this.pluralName = pluralName
    this.inGrams = inGrams
    this.id = id
  }

  static fromJson(json){
    let u = new MeasurementUnit(json.singularName, json.pluralName, json.inGrams, json.id)
    return u
  }

  convertQuantityToGrams(quantity: number){
    return quantity * this.inGrams
  }

  convertGramsToThis(quantity: number){
    return quantity / this.inGrams
  }
}