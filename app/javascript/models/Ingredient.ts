export interface Ingredient{
  id: number
  quantity: number
  name: string
  measurementUnitId: number
}