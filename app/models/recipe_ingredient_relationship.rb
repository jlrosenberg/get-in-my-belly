class RecipeIngredientRelationship < ApplicationRecord
  belongs_to :ingredient
  belongs_to :recipe
  belongs_to :measurement_unit
end
