class Ingredient < ApplicationRecord
  belongs_to :measurement_unit
  has_many :recipe_ingredient_relationships, dependent: :delete_all
end
