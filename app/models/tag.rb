class Tag < ApplicationRecord
  has_many :recipe_tag_relationships, dependent: :delete_all
end
