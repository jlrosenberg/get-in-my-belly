class Recipe < ApplicationRecord
  has_many :recipe_tag_relationships, dependent: :delete_all
  has_many :tags, through: :recipe_tag_relationships
  has_many :recipe_ingredient_relationships, dependent: :delete_all
  has_many :ingredients, through: :recipe_ingredient_relationships
  belongs_to :user, foreign_key: "author_id"
  has_many :recipe_steps, dependent: :delete_all
  accepts_nested_attributes_for :recipe_ingredient_relationships
  accepts_nested_attributes_for :recipe_tag_relationships




  def author_name
    return author.first_name + " "+ author.last_name
  end

  def author
    return User.find(author_id)
  end
end
