class RecipeIngredientRelationshipsController < ApplicationController
  before_action :set_recipe_ingredient_relationship, only: [:show, :edit, :update, :destroy]

  # GET /recipe_ingredient_relationships
  # GET /recipe_ingredient_relationships.json
  def index
    @recipe_ingredient_relationships = RecipeIngredientRelationship.all
  end

  # GET /recipe_ingredient_relationships/1
  # GET /recipe_ingredient_relationships/1.json
  def show
  end

  # GET /recipe_ingredient_relationships/new
  def new
    @recipe_ingredient_relationship = RecipeIngredientRelationship.new
  end

  # GET /recipe_ingredient_relationships/1/edit
  def edit
  end

  # POST /recipe_ingredient_relationships
  # POST /recipe_ingredient_relationships.json
  def create
    @recipe_ingredient_relationship = RecipeIngredientRelationship.new(recipe_ingredient_relationship_params)

    respond_to do |format|
      if @recipe_ingredient_relationship.save
        format.html { redirect_to @recipe_ingredient_relationship.recipe, notice: 'Recipe ingredient relationship was successfully created.' }
        format.json { render :show, status: :created, location: @recipe_ingredient_relationship }
      else
        format.html { render :new }
        format.json { render json: @recipe_ingredient_relationship.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /recipe_ingredient_relationships/1
  # PATCH/PUT /recipe_ingredient_relationships/1.json
  def update
    respond_to do |format|
      if @recipe_ingredient_relationship.update(recipe_ingredient_relationship_params)
        format.html { redirect_to @recipe_ingredient_relationship.recipe, notice: 'Recipe ingredient relationship was successfully updated.' }
        format.json { render :show, status: :ok, location: @recipe_ingredient_relationship }
      else
        format.html { render :edit }
        format.json { render json: @recipe_ingredient_relationship.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /recipe_ingredient_relationships/1
  # DELETE /recipe_ingredient_relationships/1.json
  def destroy
    @recipe_ingredient_relationship.destroy
    respond_to do |format|
      format.html { redirect_to recipe_ingredient_relationships_url, notice: 'Recipe ingredient relationship was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_recipe_ingredient_relationship
      @recipe_ingredient_relationship = RecipeIngredientRelationship.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def recipe_ingredient_relationship_params
      params.require(:recipe_ingredient_relationship).permit!
    end
end
