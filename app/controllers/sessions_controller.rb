class SessionsController < Devise::SessionsController
  skip_before_action :verify_authenticity_token


  def create
    user = User.find_by_email(sign_in_params[:email])

    if user && user.valid_password?(sign_in_params[:password])
      @jwt = user.generate_jwt
      cookies.signed[:id] = {
        value:  @jwt,
        httponly: true,
        expires: 1.hour.from_now
      }
      session[:user_id] = user.id
      session[:current_user_id] = user.id
      @current_user_id = user.id
      @current_user = user

      
    else
      render json: { errors: { 'email or password' => ['is invalid'] } }, status: :unprocessable_entity
    end
  end
end