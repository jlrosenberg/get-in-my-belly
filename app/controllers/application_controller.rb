class ApplicationController < ActionController::Base
  include ::ActionController::Cookies
  protect_from_forgery with: :null_session

  respond_to :json

  before_action :authenticate_user

  before_action :underscore_params!
  before_action :configure_permitted_parameters, if: :devise_controller?

  private 

  def underscore_params!
    params.transform_keys!(&:underscore)
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username, :first_name, :last_name, :profile_picture_url])
  end

  def authenticate_user
    if request.headers['Authorization'].present?
      authenticate_or_request_with_http_token do |token|
        begin
          jwt_payload = JWT.decode(token, Rails.application.secrets.secret_key_base).first

          @current_user_id = jwt_payload['id']
          @current_user_id = session[:current_user_id] if @current_user_id == nil
          @current_user_id = User.first.id if @current_user_id == nil
        rescue JWT::ExpiredSignature, JWT::VerificationError, JWT::DecodeError
          head :unauthorized
        end
      end
    end
  end

  def authenticate_user!(options = {})
    head :unauthorized unless signed_in?
  end

  def current_user
    @current_user ||= super || User.find(@current_user_id)
    
  end

  def current_user2
    puts session[:current_user_id].to_s
    @current_user ||= session[:current_user_id] &&
      User.find(session[:current_user_id])
  end

  def signed_in?
    @current_user_id = User.first.id unless @current_user_id.present?
    @current_user_id.present?
  end
end
