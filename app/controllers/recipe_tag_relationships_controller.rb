class RecipeTagRelationshipsController < ApplicationController
  before_action :set_recipe_tag_relationship, only: [:show, :edit, :update, :destroy]

  # GET /recipe_tag_relationships
  # GET /recipe_tag_relationships.json
  def index
    @recipe_tag_relationships = RecipeTagRelationship.all
  end

  # GET /recipe_tag_relationships/1
  # GET /recipe_tag_relationships/1.json
  def show
  end

  # GET /recipe_tag_relationships/new
  def new
    @recipe_tag_relationship = RecipeTagRelationship.new
  end

  # GET /recipe_tag_relationships/1/edit
  def edit
  end

  # POST /recipe_tag_relationships
  # POST /recipe_tag_relationships.json
  def create
    @recipe_tag_relationship = RecipeTagRelationship.new(recipe_tag_relationship_params)

    respond_to do |format|
      if @recipe_tag_relationship.save
        format.html { redirect_to @recipe_tag_relationship.recipe, notice: 'Recipe tag relationship was successfully created.' }
        format.json { render :show, status: :created, location: @recipe_tag_relationship }
      else
        format.html { render :new }
        format.json { render json: @recipe_tag_relationship.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /recipe_tag_relationships/1
  # PATCH/PUT /recipe_tag_relationships/1.json
  def update
    respond_to do |format|
      if @recipe_tag_relationship.update(recipe_tag_relationship_params)
        format.html { redirect_to @recipe_tag_relationship.recipe, notice: 'Recipe tag relationship was successfully updated.' }
        format.json { render :show, status: :ok, location: @recipe_tag_relationship }
      else
        format.html { render :edit }
        format.json { render json: @recipe_tag_relationship.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /recipe_tag_relationships/1
  # DELETE /recipe_tag_relationships/1.json
  def destroy
    @recipe_tag_relationship.destroy
    respond_to do |format|
      format.html { redirect_to recipe_tag_relationships_url, notice: 'Recipe tag relationship was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_recipe_tag_relationship
      @recipe_tag_relationship = RecipeTagRelationship.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def recipe_tag_relationship_params
      params.require(:recipe_tag_relationship).permit!
    end
end
