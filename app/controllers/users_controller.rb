class UsersController < ApplicationController
  before_action :authenticate_user!, only: [:update]

  def show
    @user = User.find(params[:id])
    @recipes = Recipe.where(author_id: @user.id)
  end

  def index
    @users = User.all
  end

  def update
    if current_user.update_attributes(user_params)
      @user = current_user
      render :show
    else
      render json: { errors: current_user.errors }, status: :unprocessable_entity
    end
  end

  # # DELETE /tags/1
  # # DELETE /tags/1.json
  # def destroy
  #   @tag.destroy
  #   respond_to do |format|
  #     format.html { redirect_to tags_url, notice: 'Tag was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end
    

  private

  def user_params
    params.require(:user).permit(:username, :email, :password, :first_name, :last_name, :profile_picture_url)
  end

  def show_params
    params.require(:id)
  end
end