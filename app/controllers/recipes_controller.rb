class RecipesController < ApplicationController
  before_action :set_recipe, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:create]


  # GET /recipes
  # GET /recipes.json
  def index
    @recipes = Recipe.all
  end

  # GET /recipes/1
  # GET /recipes/1.json
  def show
  end

  # GET /recipes/new
  def new
    @recipe = Recipe.new
  end

  # GET /recipes/1/edit
  def edit
    @recipe_tag_relationships = RecipeTagRelationship.where(recipe_id: @recipe.id)
    @recipe_ingredient_relationships = RecipeIngredientRelationship.where(recipe_id: @recipe.id)

  end

  # POST /recipes
  # POST /recipes.json
  def create

    additional_params = {
      author_id: current_user.id
    }
    @recipe = Recipe.new(author_id: current_user.id, name: recipe_params[:name], description: recipe_params[:description], banner_image_url: recipe_params[:banner_image_url])

    @ingredients = []
    @steps = []
    @tags = []
    # TODO create the recipe ingredient relationships
    recipe_params[:ingredients].each do |ingredient|
      @ingredients.push(RecipeIngredientRelationship.new(recipe: @recipe, ingredient_id: ingredient[:ingredient_id], quantity: ingredient[:quantity], measurement_unit_id: ingredient[:measurement_unit_id]))
    end unless recipe_params[:ingredients] == nil

    # TODO create the recipe steps
    recipe_params[:steps].each do |step|
      @steps.push(RecipeStep.new(recipe: @recipe, description: step[:description], order: step[:order], created_at: Time.now, updated_at: Time.now))
    end unless recipe_params[:steps] == nil

    recipe_params[:tags].each do |t|
      @tags.push(RecipeTagRelationship.new(tag_id: t))
    end unless recipe_params[:tags] == nil

    # binding.pry
    #Attempt the transaction. If it succeeds, render success message, otherwise render failure message.
    begin
      ActiveRecord::Base.transaction do
        @recipe.save!
        @ingredients.each do |ingredient|
          ingredient.recipe_id = @recipe.id
          ingredient.save!
        end
        @steps.each do |step|
          step.recipe_id = @recipe.id
          step.save!
        end

        @tags.each do |tag|
          tag.recipe_id = @recipe.id
          tag.save!
        end
      end
      respond_to do |format|
        format.html { redirect_to @recipe, notice: 'Recipe was successfully created.' }
        format.json { render :show, status: :created, location: @recipe }
      end
    rescue
      respond_to do |format|
        format.html { render :new }
        format.json { render json: @recipe.errors, status: :unprocessable_entity }
      end
    end

    # respond_to do |format|
    #   if @recipe.save
    #     format.html { redirect_to @recipe, notice: 'Recipe was successfully created.' }
    #     format.json { render :show, status: :created, location: @recipe }
    #   else
    #     format.html { render :new }
    #     format.json { render json: @recipe.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PATCH/PUT /recipes/1
  # PATCH/PUT /recipes/1.json
  def update
    respond_to do |format|
      if @recipe.update(recipe_params)
        format.html { redirect_to @recipe, notice: 'Recipe was successfully updated.' }
        format.json { render :show, status: :ok, location: @recipe }
      else
        format.html { render :edit }
        format.json { render json: @recipe.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /recipes/1
  # DELETE /recipes/1.json
  def destroy
    @recipe.destroy
    respond_to do |format|
      format.html { redirect_to recipes_url, notice: 'Recipe was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_recipe
      @recipe = Recipe.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def recipe_params
      # params.require(:recipe).permit(:name, :description, :banner_image_url, :ingredients => [:ingredient_id, :quantity, :measurement_unit_id], :steps => [:order, :description])
      params.require(:recipe).permit!
    end
end
